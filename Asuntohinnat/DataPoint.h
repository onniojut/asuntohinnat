//
//  DataPoint.h
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 8.9.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataPoint : NSObject{
    int _postcode;
    int _year;
    int _quarter;
    int _apartment_type;
    int _apartment_age;
    int _mean_price;
    int _count;
}

@property (atomic, assign) int postcode;
@property (atomic, assign) int year;
@property (atomic, assign) int quarter;
@property (atomic, assign) int apartment_type;
@property (atomic, assign) int apartment_age;
@property (atomic, assign) int mean_price;
@property (atomic, assign) int count;

-(id) initWithValuesFromDictionary:(NSDictionary*)dict;
-(void) setValuesFromDictionary:(NSDictionary*)dict;

@end
