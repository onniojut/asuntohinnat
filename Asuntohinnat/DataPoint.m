//
//  DataPoint.m
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 8.9.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//

#import "DataPoint.h"

@implementation DataPoint
@synthesize postcode = _postcode;
@synthesize year = _year;
@synthesize quarter = _quarter;
@synthesize apartment_age = _apartment_age;
@synthesize apartment_type = _apartment_type;
@synthesize mean_price = _mean_price;
@synthesize count = _count;

- (id)init{
    if(self = [super init]){
        self.postcode = -1;
        self.year = -1;
        self.quarter = -1;
        self.apartment_age = -1;
        self.apartment_type = -1;
        self.mean_price = -1;
        self.count = -1;
    }
    return self;
}


-(id) initWithValuesFromDictionary:(NSDictionary*)dict
{
    if(self = [super init]){
        self.postcode = [[dict objectForKey:@"postcode"] intValue];
        self.year = [[dict objectForKey:@"year"] intValue];
        self.quarter = [[dict objectForKey:@"quarter"] intValue];
        self.apartment_age = [[dict objectForKey:@"age"] intValue];
        self.apartment_type = [[dict objectForKey:@"type"] intValue];
        self.mean_price = [[dict objectForKey:@"price"] intValue];
        self.count = [[dict objectForKey:@"count"] intValue];
    }
    return self;
}

-(void) setValuesFromDictionary:(NSDictionary*)dict
{
    self.postcode = [[dict objectForKey:@"postcode"] intValue];
    self.year = [[dict objectForKey:@"year"] intValue];
    self.quarter = [[dict objectForKey:@"quarter"] intValue];
    self.apartment_age = [[dict objectForKey:@"age"] intValue];
    self.apartment_type = [[dict objectForKey:@"type"] intValue];
    self.mean_price = [[dict objectForKey:@"price"] intValue];
    self.count = [[dict objectForKey:@"count"] intValue];
}

@end
