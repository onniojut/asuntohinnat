//
//  DataManager.m
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 20.8.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//

#import "DataManager.h"
#import "CHCSVParser/CHCSV.h"

@implementation DataManager

@synthesize year = _year;
@synthesize age = _age;
@synthesize postcode = _postcode;
@synthesize price = _price;
@synthesize count = _count;

-(id)init
{
    if(self = [super init]){
        // custom init
        task = -1;
        _year = @"";
        _age = @"";
        _postcode = @"";
        _price = @"";
        _count = @"";
    }
    return self;
}

- (NSMutableArray*)postcodeStrings
{
    postcodes = [NSMutableArray arrayWithCapacity:0];
    
    @synchronized(self){
        task = TASK_GET_POSTCODESTRINGS;
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"postcodes-2012" ofType:@"csv"];
        
        NSError *err = nil;
        CHCSVParser *parser = [[CHCSVParser alloc] initWithContentsOfCSVFile:path encoding:NSUTF8StringEncoding error:&err];
        [parser setDelimiter:@";"];
        [parser setParserDelegate:self];
        
        if(err){
            NSLog(@"parser init error %@", [err description]);
            return postcodes;
        }
        
        row = 0;
        col = 0;
        
        
        [parser parse];
    }
    
    return postcodes;
}

- (NSMutableDictionary*)valuesForPostcode:(NSString*)postcode age:(NSString*)age
{
    values = [NSMutableDictionary dictionaryWithCapacity:0];
    
    @synchronized(self){
        task = TASK_GET_VALUES_FOR_POSTCODE_AND_AGE;
        
        searchedPostcode = postcode;
        searchedAge = age;
        
        NSString *path = [[NSBundle mainBundle] pathForResource:searchedAge ofType:@"csv"];
        
        NSError *err = nil;
        CHCSVParser *parser = [[CHCSVParser alloc] initWithContentsOfCSVFile:path encoding:NSUTF8StringEncoding error:&err];
        [parser setDelimiter:@";"];
        [parser setParserDelegate:self];
        
        if(err){
            NSLog(@"parser init error %@", [err description]);
            [values setObject:@"failed: parser init error" forKey:@"status"];
            return values;
        }
        
        row = 0;
        col = 0;
        
        
        [parser parse];
    }
    
    return values;
}


#pragma mark CHCSVParserDelegate
- (void) parser:(CHCSVParser *)parser didStartDocument:(NSString *)csvFile
{
    //NSLog(@"parser didStartDocument %@", csvFile);
}

- (void) parser:(CHCSVParser *)parser didStartLine:(NSUInteger)lineNumber
{
    //NSLog(@"parser didStartLine %d", lineNumber);
    row = lineNumber;
    
}

- (void)parser:(CHCSVParser *)parser didEndLine:(NSUInteger)lineNumber
{
    //NSLog(@"parser didEndLine %d", lineNumber);
    switch (task) {
        case TASK_GET_VALUES_FOR_POSTCODE_AND_AGE:
            if([searchedPostcode isEqualToString:self.postcode]){
                // found the right postcode, lets get the values and stop the parsing
                
                if(![self.price isEqualToString:@"."] && ![self.count isEqualToString:@"."]){
                    [values setObject:self.price forKey:@"price"];
                    [values setObject:self.count forKey:@"count"];
                    [values setObject:@"success" forKey:@"status"];
                }
                else{
                    
                    [values setObject:@"no-data-for-postcode" forKey:@"status"];
                }
                
                [parser cancelParsing];
            };
            break;
            
        default:
            break;
    }
    col = 0;
}

- (void) parser:(CHCSVParser *)parser didReadField:(NSString *)field
{
    switch (task) {
        case TASK_GET_POSTCODESTRINGS:
            if(col == 2){
                //NSLog(@"parser didReadField %@", field);
                [postcodes addObject:field];
            }
            break;
        case TASK_GET_VALUES_FOR_POSTCODE_AND_AGE:
            // "2012";"1980-1989";"00440";3155;22
            switch (col) {
                case 0:
                    self.year = field;
                    break;
                case 1:
                    self.age = field;
                    break;
                case 2:
                    self.postcode = field;
                    break;
                case 3:
                    self.price = field;
                    break;
                case 4:
                    self.count = field;
                    break;
                default:
                    break;
            }
            break;
            
        default:
            break;
    }
    
    col++;
}

- (void) parser:(CHCSVParser *)parser didEndDocument:(NSString *)csvFile
{
    switch (task) {
        case TASK_GET_VALUES_FOR_POSTCODE_AND_AGE:
            if([values objectForKey:@"status"] == nil){
                [values setObject:@"no-postcode-found" forKey:@"status"];
            }
            break;
            
        default:
            break;
    }

    
    task = -1;
}

- (void) parser:(CHCSVParser *)parser didFailWithError:(NSError *)error
{
    switch (task) {
        case TASK_GET_VALUES_FOR_POSTCODE_AND_AGE:
            if([values objectForKey:@"status"] == nil){
                [values setObject:@"no-postcode-found" forKey:@"status"];
            }
            break;
            
        default:
            break;
    }

    
    task = -1;
}

@end
