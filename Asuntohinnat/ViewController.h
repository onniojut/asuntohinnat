//
//  ViewController.h
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 16.8.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultViewController.h"

@interface ViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate>{
    IBOutlet UITextField *postCodeTextField;
    NSUInteger _selectedAge;
    NSUInteger _selectedType;
    BOOL runningRequest;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UIView *indicatorBackgroundView;
}

@property (nonatomic, assign) NSUInteger selectedAge;
@property (nonatomic, assign) NSUInteger selectedType;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (strong, nonatomic) IBOutlet UIPickerView *agePicker;

- (IBAction)searchButtonPressed:(id)sender;
- (IBAction)postcodeEditingDidEnd:(id)sender;
- (IBAction)touchOnBackground:(id)sender;

@end
