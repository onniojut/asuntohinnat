//
//  ResultViewController.h
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 17.8.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataPoint.h"

@interface ResultViewController : UIViewController {
    NSMutableArray *datapoints;
    NSMutableArray *annuals;
    DataPoint *infoDatapoint;
    NSString *copyrightString;
}

@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UISlider *apartmentSizeSlider;
@property (strong, nonatomic) IBOutlet UILabel *label3;
@property (strong, nonatomic) IBOutlet UILabel *meanPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *sizeLabel;
@property (strong, nonatomic) IBOutlet UILabel *copyrightLabel;
@property (strong, nonatomic) IBOutlet UITextField *sizeTextField;

@property (nonatomic, retain) NSString* copyrightString;
@property (nonatomic, retain) NSMutableArray *datapoints;

- (IBAction)aparmentSizeValueChanged:(id)sender;
- (void) statButtonPressed;

@end
