//
//  DataManager.h
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 20.8.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CHCSVParser/CHCSV.h"

#define APARTMENT_AGE_BELOW_1950 0
#define APARTMENT_AGE_50_to_59   1
#define APARTMENT_AGE_60_to_69   2
#define APARTMENT_AGE_70_to_79   3
#define APARTMENT_AGE_80_to_89   4
#define APARTMENT_AGE_90_to_99   5
#define APARTMENT_AGE_ABOVE_00   6
#define APARTMENT_AGE_ALL        7

#define TASK_GET_POSTCODESTRINGS 0
#define TASK_GET_VALUES_FOR_POSTCODE_AND_AGE 1

@interface DataManager : NSObject <CHCSVParserDelegate>
{
    //CHCSVParser *parser;
    int row;
    int col;
    
    int task;
    
    // TASK_GET_POSTCODESSTRINGS
    NSMutableArray *postcodes;
    
    // TASK_GET_VALUES_FOR_POSTCODE_AND_AGE
    NSString *searchedPostcode;
    NSString *searchedAge;
    NSString *_year;
    NSString *_age;
    NSString *_postcode;
    NSString *_price;
    NSString *_count;
    NSMutableDictionary *values;
    
}

@property (nonatomic, retain) NSString *year;
@property (nonatomic, retain) NSString *age;
@property (nonatomic, retain) NSString *postcode;
@property (nonatomic, retain) NSString *price;
@property (nonatomic, retain) NSString *count;

- (id) init;
- (NSMutableArray*)postcodeStrings;
- (NSMutableDictionary*)valuesForPostcode:(NSString*)postcode age:(NSString*)age;

@end
