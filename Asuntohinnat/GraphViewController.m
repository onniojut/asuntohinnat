//
//  GraphViewController.m
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 17.9.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//

#import "GraphViewController.h"
#import "DataPoint.h"

@interface GraphViewController ()

@end

@implementation GraphViewController

@synthesize quartersGraphHostingView;
@synthesize annualsGraphHostingView;
@synthesize annuals;
@synthesize quarters;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UISegmentedControl *segmentedButton = [[UISegmentedControl alloc] initWithItems:[NSArray
                                                                                     arrayWithObjects:@"Vuodet", @"Neljännekset", nil]];
    [segmentedButton setTintColor:[UIColor colorWithRed:0.65f green:0.65f blue:0.65f alpha:1.0f]];
    [segmentedButton addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    //    segmentedButton.frame = CGRectMake(0, 0, 90, 35);
    segmentedButton.segmentedControlStyle = UISegmentedControlStyleBar;
    segmentedButton.momentary = NO;
    [segmentedButton setSelectedSegmentIndex:0];
    
    self.navigationItem.titleView = segmentedButton;
    
    [self setupQuartersGraph];
    [self setupAnnualsGraph];
    
    // show annuals graph
    [[self quartersGraphHostingView] setHidden:YES];
    [[self annualsGraphHostingView] setHidden:NO];
}

- (void)viewDidUnload
{
    [self setAnnualsGraphHostingView:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [self setQuartersGraphHostingView:nil];
    self.annuals = nil;
    self.quarters = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)segmentAction:(id)sender{
    
    if([sender selectedSegmentIndex] == 1){
        // show quarters graph
        [[self quartersGraphHostingView] setHidden:NO];
        [[self annualsGraphHostingView] setHidden:YES];
    }else{
        // show annuals graph
        [[self quartersGraphHostingView] setHidden:YES];
        [[self annualsGraphHostingView] setHidden:NO];
    }
}

- (void)setupQuartersGraph
{
    
    quartersStartQuarter = 5, quartersStartYear = 2100;
    int endQuarter = -1, endYear = 1900;
    int minPrice = INT_MAX;
    int maxPrice = INT_MIN;
    int count;
    if(quarters.count > 0){
        for(DataPoint *dp in quarters){
            if(dp.year < quartersStartYear) quartersStartYear = dp.year;
            if(dp.year > endYear) endYear = dp.year;
            if(dp.mean_price > maxPrice) maxPrice = dp.mean_price;
            if(dp.mean_price < minPrice) minPrice = dp.mean_price;
        }
        for(DataPoint *dp in quarters){
            if(dp.year == quartersStartYear && dp.quarter < quartersStartQuarter) quartersStartQuarter = dp.quarter;
            if(dp.year == endYear && dp.quarter > endQuarter) endQuarter = dp.quarter;
        }
        
        count = (endYear - quartersStartYear - 1) * 4; // full years
        if(count < 0) count = 0;
        count += 5 - quartersStartQuarter;
        count += endQuarter;
    }
    else{
        quartersStartYear = endYear = 2005;
        quartersStartQuarter = endQuarter = 1;
        minPrice = 1000;
        maxPrice = 2000;
        
        count = 0;
    }
    
    NSLog(@"first year:%d, quarter:%d", quartersStartYear, quartersStartQuarter);
    
    // Create graph from theme
    quartersGraph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [quartersGraph applyTheme:theme];
    self.quartersGraphHostingView.collapsesLayers = NO; // Setting to YES reduces GPU memory usage, but can slow drawing/scrolling
    self.quartersGraphHostingView.hostedGraph = quartersGraph;
    
    quartersGraph.paddingLeft   = 5.0;
    quartersGraph.paddingTop    = 5.0;
    quartersGraph.paddingRight  = 5.0;
    quartersGraph.paddingBottom = 5.0;
    
    // Setup plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)quartersGraph.defaultPlotSpace;
    [plotSpace setDelegate:self];
    plotSpace.allowsUserInteraction = YES;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0) length:CPTDecimalFromFloat(5.0)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minPrice - 1000) length:CPTDecimalFromFloat(maxPrice+1000)];
    plotSpace.globalXRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1.0) length:CPTDecimalFromFloat(count + 2)];
    
    NSLog(@"Y range: [%d, %d]", minPrice, maxPrice);
    
    // Axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)quartersGraph.axisSet;
    CPTXYAxis *x          = axisSet.xAxis;
    x.majorIntervalLength         = CPTDecimalFromFloat(1.0);
    x.orthogonalCoordinateDecimal = CPTDecimalFromFloat(minPrice);
    x.minorTicksPerInterval       = 0;
    x.axisConstraints = [CPTConstraints constraintWithLowerOffset:30.0f];
    x.majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    
    // Define some custom labels for the data elements
    //x.labelRotation = M_PI/4;
    //x.labelOffset = 0.0f;
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    NSMutableArray *xAxisLabels = [NSMutableArray arrayWithCapacity:0];
    NSMutableSet *majorTickLocations = [NSMutableSet setWithCapacity:0];
    int i = 0;
    int quarter = quartersStartQuarter;
    int year = quartersStartYear;
    for(int index = 0; index < count; index++){
        NSString *labelStr = [NSString stringWithFormat:@"Q%d/%02d", quarter, year - 2000];
        CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:labelStr textStyle:x.labelTextStyle];
        label.offset = 0.0f;
        label.tickLocation = [[NSNumber numberWithInt:i] decimalValue];
        [majorTickLocations addObject:[NSDecimalNumber numberWithInt:i]];
        [xAxisLabels addObject:label];
        i++;
        quarter++;
        if(quarter > 4){
            year++;
            quarter = 1;
        }
    }
    
    x.majorTickLocations = majorTickLocations;
    x.axisLabels =  [NSSet setWithArray:xAxisLabels];
    
    //    NSArray *exclusionRanges = [NSArray arrayWithObjects:
    //                                [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(1.99) length:CPTDecimalFromFloat(0.02)],
    //                                [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.99) length:CPTDecimalFromFloat(0.02)],
    //                                [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(2.99) length:CPTDecimalFromFloat(0.02)],
    //                                nil];
    //    x.labelExclusionRanges = exclusionRanges;
    
    CPTXYAxis *y = axisSet.yAxis;
    y.majorIntervalLength         = CPTDecimalFromFloat(1000.0);
    y.minorTicksPerInterval       = 1;
    y.orthogonalCoordinateDecimal = CPTDecimalFromFloat(0.0);
    y.axisConstraints = [CPTConstraints constraintWithUpperOffset:10.0f];
    y.majorGridLineStyle = [CPTLineStyle lineStyle];
    //    exclusionRanges               = [NSArray arrayWithObjects:
    //                                     [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(1.99) length:CPTDecimalFromFloat(0.02)],
    //                                     [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.99) length:CPTDecimalFromFloat(0.02)],
    //                                     [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(3.99) length:CPTDecimalFromFloat(0.02)],
    //                                     nil];
    //    y.labelExclusionRanges = exclusionRanges;
    //y.delegate             = self;
    
    // Create a blue plot area
    CPTScatterPlot *boundLinePlot  = [[CPTScatterPlot alloc] init];
    CPTMutableLineStyle *lineStyle = [CPTMutableLineStyle lineStyle];
    lineStyle.miterLimit        = 1.0f;
    lineStyle.lineWidth         = 3.0f;
    lineStyle.lineColor         = [CPTColor colorWithComponentRed:1.0f green:0.55f blue:0.0f alpha:1.0f];
    boundLinePlot.dataLineStyle = lineStyle;
    boundLinePlot.identifier    = @"quarters_price";
    boundLinePlot.dataSource    = self;
    [quartersGraph addPlot:boundLinePlot];
    
    // Do a blue gradient
    CPTColor *areaColor1       = [CPTColor colorWithComponentRed:1.0f green:0.55f blue:0.0f alpha:0.8f];
    CPTGradient *areaGradient1 = [CPTGradient gradientWithBeginningColor:areaColor1 endingColor:[CPTColor clearColor]];
    areaGradient1.angle = -90.0f;
    CPTFill *areaGradientFill = [CPTFill fillWithGradient:areaGradient1];
    boundLinePlot.areaFill      = areaGradientFill;
    boundLinePlot.areaBaseValue = [[NSDecimalNumber zero] decimalValue];
    
    // Add plot symbols
    CPTMutableLineStyle *symbolLineStyle = [CPTMutableLineStyle lineStyle];
    symbolLineStyle.lineColor = [CPTColor blackColor];
    CPTPlotSymbol *plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    plotSymbol.fill          = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:1.0f green:0.45f blue:0.0f alpha:1.0f]];
    plotSymbol.lineStyle     = symbolLineStyle;
    plotSymbol.size          = CGSizeMake(10.0, 10.0);
    boundLinePlot.plotSymbol = plotSymbol;
    
    //    // Create a green plot area
    //    CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] init];
    //    lineStyle                        = [CPTMutableLineStyle lineStyle];
    //    lineStyle.lineWidth              = 3.f;
    //    lineStyle.lineColor              = [CPTColor greenColor];
    //    lineStyle.dashPattern            = [NSArray arrayWithObjects:[NSNumber numberWithFloat:5.0f], [NSNumber numberWithFloat:5.0f], nil];
    //    dataSourceLinePlot.dataLineStyle = lineStyle;
    //    dataSourceLinePlot.identifier    = @"Green Plot";
    //    dataSourceLinePlot.dataSource    = self;
    //
    //    // Put an area gradient under the plot above
    //    CPTColor *areaColor       = [CPTColor colorWithComponentRed:0.3 green:1.0 blue:0.3 alpha:0.8];
    //    CPTGradient *areaGradient = [CPTGradient gradientWithBeginningColor:areaColor endingColor:[CPTColor clearColor]];
    //    areaGradient.angle               = -90.0f;
    //    areaGradientFill                 = [CPTFill fillWithGradient:areaGradient];
    //    dataSourceLinePlot.areaFill      = areaGradientFill;
    //    dataSourceLinePlot.areaBaseValue = CPTDecimalFromString(@"1.75");
    //
    //    // Animate in the new plot, as an example
    //    dataSourceLinePlot.opacity = 0.0f;
    //    [graph addPlot:dataSourceLinePlot];
    //
    //    CABasicAnimation *fadeInAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    //    fadeInAnimation.duration            = 1.0f;
    //    fadeInAnimation.removedOnCompletion = NO;
    //    fadeInAnimation.fillMode            = kCAFillModeForwards;
    //    fadeInAnimation.toValue             = [NSNumber numberWithFloat:1.0];
    //    [dataSourceLinePlot addAnimation:fadeInAnimation forKey:@"animateOpacity"];
    
    // scale the plot
    //[plotSpace scaleToFitPlots:[graph allPlots]];
    //[graph.defaultPlotSpace scaleBy:0.5 aboutPoint:CGPointMake(0.0, 0.0)];
    //    CPTMutablePlotRange *xrange = [plotSpace.xRange mutableCopy];
}

-(void)setupAnnualsGraph
{
    annualsStartYear = 3000;
    int minPrice = INT_MAX;
    int maxPrice = INT_MIN;
    if(annuals.count > 0){
        for(DataPoint *dp in annuals){
            if(dp.year < annualsStartYear) annualsStartYear = dp.year;
        }
        for(DataPoint *dp in annuals){
            if(dp.mean_price > maxPrice) maxPrice = dp.mean_price;
            if(dp.mean_price < minPrice) minPrice = dp.mean_price;
        }
    }
    else{
        annualsStartYear = 2005;
        minPrice = 1000;
        maxPrice = 2000;
    }
    
    // Create graph from theme
    annualsGraph = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    CPTTheme *theme = [CPTTheme themeNamed:kCPTDarkGradientTheme];
    [annualsGraph applyTheme:theme];
    self.annualsGraphHostingView.collapsesLayers = NO; // Setting to YES reduces GPU memory usage, but can slow drawing/scrolling
    self.annualsGraphHostingView.hostedGraph = annualsGraph;
    
    annualsGraph.paddingLeft   = 5.0;
    annualsGraph.paddingTop    = 5.0;
    annualsGraph.paddingRight  = 5.0;
    annualsGraph.paddingBottom = 5.0;
    
    // Setup plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)annualsGraph.defaultPlotSpace;
    [plotSpace setDelegate:self];
    plotSpace.allowsUserInteraction = YES;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.0) length:CPTDecimalFromFloat(5.0)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minPrice - 1000) length:CPTDecimalFromFloat(maxPrice+1000)];
    plotSpace.globalXRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1.0) length:CPTDecimalFromFloat(annuals.count + 2)];
    
    NSLog(@"Y range: [%d, %d]", minPrice, maxPrice);
    
    // Axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)annualsGraph.axisSet;
    CPTXYAxis *x          = axisSet.xAxis;
    x.majorIntervalLength         = CPTDecimalFromFloat(1.0);
    x.orthogonalCoordinateDecimal = CPTDecimalFromFloat(minPrice);
    x.minorTicksPerInterval       = 0;
    x.axisConstraints = [CPTConstraints constraintWithLowerOffset:30.0f];
    x.majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    
    // Define some custom labels for the data elements
    //x.labelRotation = M_PI/4;
    //x.labelOffset = 0.0f;
    x.labelingPolicy = CPTAxisLabelingPolicyNone;
    NSMutableArray *xAxisLabels = [NSMutableArray arrayWithCapacity:0];
    NSMutableSet *majorTickLocations = [NSMutableSet setWithCapacity:0];
    int i = 0;
    for(DataPoint *dp in annuals){
        NSString *labelStr = [NSString stringWithFormat:@"%d", dp.year];
        CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:labelStr textStyle:x.labelTextStyle];
        label.offset = 0.0f;
        label.tickLocation = [[NSNumber numberWithInt:i] decimalValue];
        [majorTickLocations addObject:[NSDecimalNumber numberWithInt:i]];
        [xAxisLabels addObject:label];
        i++;
    }
    
    x.majorTickLocations = majorTickLocations;
    x.axisLabels =  [NSSet setWithArray:xAxisLabels];
    
    //    NSArray *exclusionRanges = [NSArray arrayWithObjects:
    //                                [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(1.99) length:CPTDecimalFromFloat(0.02)],
    //                                [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.99) length:CPTDecimalFromFloat(0.02)],
    //                                [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(2.99) length:CPTDecimalFromFloat(0.02)],
    //                                nil];
    //    x.labelExclusionRanges = exclusionRanges;
    
    CPTXYAxis *y = axisSet.yAxis;
    y.majorIntervalLength         = CPTDecimalFromFloat(1000.0);
    y.minorTicksPerInterval       = 1;
    y.orthogonalCoordinateDecimal = CPTDecimalFromFloat(0.0);
    y.axisConstraints = [CPTConstraints constraintWithUpperOffset:10.0f];
    y.majorGridLineStyle = [CPTLineStyle lineStyle];
    //    exclusionRanges               = [NSArray arrayWithObjects:
    //                                     [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(1.99) length:CPTDecimalFromFloat(0.02)],
    //                                     [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0.99) length:CPTDecimalFromFloat(0.02)],
    //                                     [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(3.99) length:CPTDecimalFromFloat(0.02)],
    //                                     nil];
    //    y.labelExclusionRanges = exclusionRanges;
    //y.delegate             = self;
    
    // Create a blue plot area
    CPTScatterPlot *boundLinePlot  = [[CPTScatterPlot alloc] init];
    CPTMutableLineStyle *lineStyle = [CPTMutableLineStyle lineStyle];
    lineStyle.miterLimit        = 1.0f;
    lineStyle.lineWidth         = 3.0f;
    lineStyle.lineColor         = [CPTColor colorWithComponentRed:1.0f green:0.55f blue:0.0f alpha:1.0f];
    boundLinePlot.dataLineStyle = lineStyle;
    boundLinePlot.identifier    = @"annuals_price";
    boundLinePlot.dataSource    = self;
    [annualsGraph addPlot:boundLinePlot];
    
    // Do a blue gradient
    CPTColor *areaColor1       = [CPTColor colorWithComponentRed:1.0f green:0.55f blue:0.0f alpha:0.8f];
    CPTGradient *areaGradient1 = [CPTGradient gradientWithBeginningColor:areaColor1 endingColor:[CPTColor clearColor]];
    areaGradient1.angle = -90.0f;
    CPTFill *areaGradientFill = [CPTFill fillWithGradient:areaGradient1];
    boundLinePlot.areaFill      = areaGradientFill;
    boundLinePlot.areaBaseValue = [[NSDecimalNumber zero] decimalValue];
    
    // Add plot symbols
    CPTMutableLineStyle *symbolLineStyle = [CPTMutableLineStyle lineStyle];
    symbolLineStyle.lineColor = [CPTColor blackColor];
    CPTPlotSymbol *plotSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    plotSymbol.fill          = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:1.0f green:0.45f blue:0.0f alpha:1.0f]];
    plotSymbol.lineStyle     = symbolLineStyle;
    plotSymbol.size          = CGSizeMake(10.0, 10.0);
    boundLinePlot.plotSymbol = plotSymbol;
    
    //    // Create a green plot area
    //    CPTScatterPlot *dataSourceLinePlot = [[CPTScatterPlot alloc] init];
    //    lineStyle                        = [CPTMutableLineStyle lineStyle];
    //    lineStyle.lineWidth              = 3.f;
    //    lineStyle.lineColor              = [CPTColor greenColor];
    //    lineStyle.dashPattern            = [NSArray arrayWithObjects:[NSNumber numberWithFloat:5.0f], [NSNumber numberWithFloat:5.0f], nil];
    //    dataSourceLinePlot.dataLineStyle = lineStyle;
    //    dataSourceLinePlot.identifier    = @"Green Plot";
    //    dataSourceLinePlot.dataSource    = self;
    //
    //    // Put an area gradient under the plot above
    //    CPTColor *areaColor       = [CPTColor colorWithComponentRed:0.3 green:1.0 blue:0.3 alpha:0.8];
    //    CPTGradient *areaGradient = [CPTGradient gradientWithBeginningColor:areaColor endingColor:[CPTColor clearColor]];
    //    areaGradient.angle               = -90.0f;
    //    areaGradientFill                 = [CPTFill fillWithGradient:areaGradient];
    //    dataSourceLinePlot.areaFill      = areaGradientFill;
    //    dataSourceLinePlot.areaBaseValue = CPTDecimalFromString(@"1.75");
    //
    //    // Animate in the new plot, as an example
    //    dataSourceLinePlot.opacity = 0.0f;
    //    [graph addPlot:dataSourceLinePlot];
    //
    //    CABasicAnimation *fadeInAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    //    fadeInAnimation.duration            = 1.0f;
    //    fadeInAnimation.removedOnCompletion = NO;
    //    fadeInAnimation.fillMode            = kCAFillModeForwards;
    //    fadeInAnimation.toValue             = [NSNumber numberWithFloat:1.0];
    //    [dataSourceLinePlot addAnimation:fadeInAnimation forKey:@"animateOpacity"];
    
    // scale the plot
    //[plotSpace scaleToFitPlots:[graph allPlots]];
    //[graph.defaultPlotSpace scaleBy:0.5 aboutPoint:CGPointMake(0.0, 0.0)];
    //    CPTMutablePlotRange *xrange = [plotSpace.xRange mutableCopy];

}

#pragma mark -
#pragma mark Plot Data Source Methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    if([plot.identifier isEqual:@"quarters_price"]){
        return [quarters count];
    }
    else if([plot.identifier isEqual:@"annuals_price"]) {
        return [annuals count];
    }
    else{
        return 0;
    }
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    switch (fieldEnum) {
        case CPTScatterPlotFieldX:
            if([plot.identifier isEqual:@"quarters_price"]){
                DataPoint *dp = [self.quarters objectAtIndex:index];
                int count = 0;
                if(dp.year == quartersStartYear){
                    count += dp.quarter - quartersStartQuarter;
                }
                else{
                    count += (dp.year - quartersStartYear) * 4; // full years
                    if(count < 0) count = 0;
                    //count += 5 - quartersStartQuarter; // first year
                    count += dp.quarter - 1; // last year
                }
//                NSLog(@"DEBUG:   %d, Q%d/%d, %d", index, dp.quarter, dp.year, count);
                
                return [NSDecimalNumber numberWithInt:count];
            }
            else if ([plot.identifier isEqual:@"annuals_price"]){
                
                return [NSDecimalNumber numberWithInt:index];
            }
            break;
        case CPTScatterPlotFieldY:
            if([plot.identifier isEqual:@"quarters_price"]){
                DataPoint *dp = [self.quarters objectAtIndex:index];
                return [NSDecimalNumber numberWithInt:dp.mean_price];
            }
            else if([plot.identifier isEqual:@"annuals_price"]) {
                DataPoint *dp = [self.annuals objectAtIndex:index];
                return [NSDecimalNumber numberWithInt:dp.mean_price];
            }
            else{
                return 0;
            }
            break;
            
        default:
            return [NSDecimalNumber numberWithInt:0];
            break;
    }
    return 0;
}

#pragma mark -
#pragma mark Axis Delegate Methods

-(BOOL)axis:(CPTAxis *)axis shouldUpdateAxisLabelsAtLocations:(NSSet *)locations
{
    //    static CPTTextStyle *positiveStyle = nil;
    //    static CPTTextStyle *negativeStyle = nil;
    //
    //    NSNumberFormatter *formatter = axis.labelFormatter;
    //    CGFloat labelOffset          = axis.labelOffset;
    //    NSDecimalNumber *zero        = [NSDecimalNumber zero];
    //
    //    NSMutableSet *newLabels = [NSMutableSet set];
    //
    //    for ( NSDecimalNumber *tickLocation in locations ) {
    //        CPTTextStyle *theLabelTextStyle;
    //
    //        if ( [tickLocation isGreaterThanOrEqualTo:zero] ) {
    //            if ( !positiveStyle ) {
    //                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
    //                newStyle.color = [CPTColor greenColor];
    //                positiveStyle  = newStyle;
    //            }
    //            theLabelTextStyle = positiveStyle;
    //        }
    //        else {
    //            if ( !negativeStyle ) {
    //                CPTMutableTextStyle *newStyle = [axis.labelTextStyle mutableCopy];
    //                newStyle.color = [CPTColor redColor];
    //                negativeStyle  = newStyle;
    //            }
    //            theLabelTextStyle = negativeStyle;
    //        }
    //
    //        NSString *labelString       = [formatter stringForObjectValue:tickLocation];
    //        CPTTextLayer *newLabelLayer = [[CPTTextLayer alloc] initWithText:labelString style:theLabelTextStyle];
    //
    //        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithContentLayer:newLabelLayer];
    //        newLabel.tickLocation = tickLocation.decimalValue;
    //        newLabel.offset       = labelOffset;
    //
    //        [newLabels addObject:newLabel];
    //
    //        [newLabel release];
    //        [newLabelLayer release];
    //    }
    //
    //    axis.axisLabels = newLabels;
    
    return NO;
}

-(CGPoint)plotSpace:(CPTPlotSpace *)space willDisplaceBy:(CGPoint)proposedDisplacementVector
{
    return CGPointMake(proposedDisplacementVector.x, 0);
}

@end
