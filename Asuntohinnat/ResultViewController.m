//
//  ResultViewController.m
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 17.8.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//

#import "ResultViewController.h"
#import "DataPoint.h"
#import "GraphViewController.h"

@interface ResultViewController ()

@end

@implementation ResultViewController
@synthesize label1;
@synthesize label2;
@synthesize apartmentSizeSlider;
@synthesize label3;
@synthesize datapoints;
@synthesize copyrightString;
@synthesize sizeTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.datapoints = nil;
        self.copyrightString = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
//    [self.graphHostingView setHidden:YES];
    
    // right bar button
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"stat"
                                                               style:UIBarButtonItemStyleBordered
                                                              target:self
                                                              action:@selector(statButtonPressed)];
    [button setImage:[UIImage imageNamed:@"stat-btn"]];
    [button setTintColor:[UIColor colorWithRed:0.65f green:0.65f blue:0.65f alpha:1.0f]];
    self.navigationItem.rightBarButtonItem = button;
    
    [self registerForKeyboardNotifications];
    
    if(datapoints != nil){
        // sort by year and quarter
        [datapoints sortUsingComparator:^(id firstObject, id secondObject) {
            DataPoint *first = (DataPoint*)firstObject;
            DataPoint *second = (DataPoint*)secondObject;
            if (first.year < second.year ||
                (first.year == second.year && first.quarter < second.quarter))
                return NSOrderedAscending;
            else if (first.year == second.year && first.quarter == second.quarter)
                return NSOrderedSame;
            else
                return NSOrderedDescending;
        }];
        
        [self setupMainData:[datapoints lastObject]];
        
        // filter out annual records
        annuals = [NSMutableArray arrayWithCapacity:0];
        for(DataPoint *dp in datapoints){
            if(dp.quarter == 0){
                [annuals addObject:dp];
            }
        }
        [datapoints removeObjectsInArray:annuals];
        
    }
}

- (void)viewWillAppear:(BOOL)animated
{
}

- (void)setupMainData:(DataPoint*)dp
{
    infoDatapoint = dp;
    
    // view title
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:20.0f];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text = [NSString stringWithFormat:@"%05d", dp.postcode ];
    self.navigationItem.titleView = label;
    [self.navigationItem.titleView sizeToFit];
    
    if(dp.quarter != 0){
        NSString *string = [NSString stringWithFormat:@"Postinumeroalueella %05d tehtiin vuoden %d %@ neljänneksellä %d asuntokauppaa, joiden keskimääräinen neliöhinta on:", dp.postcode, dp.year, [self stringLiteralForQuarter:dp.quarter ], dp.count];
        [label1 setText:string];
    }
    else{
        NSString *string = [NSString stringWithFormat:@"Postinumeroalueella %05d tehtiin vuonna %d %d asuntokauppaa, joiden keskimääräinen neliöhinta on:", dp.postcode, dp.year, dp.count];
        [label1 setText:string];
    }

    [label2 setText:[NSString stringWithFormat:@"%d", dp.mean_price]];
    
    float value = [apartmentSizeSlider value];
    [label3 setText:[NSString stringWithFormat:@"%d", dp.mean_price * (int)value]];
    
    [self.meanPriceLabel setText:@"€/m\u00B2"];
    [self.sizeLabel setText:@"m\u00B2"];
    
    if(copyrightString != nil){
        [self.copyrightLabel setText:copyrightString];
    }
    else{
        [self.copyrightLabel setText:@"\u00A9 Tilastokeskus"];
    }
    
    [sizeTextField setText:@"50"];
    [apartmentSizeSlider setValue:50.0f];
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWillShow:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = CGRectMake(0, -kbSize.height, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }];
}

- (NSString*)stringLiteralForQuarter:(int)quarter
{
    switch (quarter) {
        case 1:
            return @"ensimmäisellä";
            break;
        case 2:
            return @"toisella";
            break;
        case 3:
            return @"kolmannella";
            break;
        case 4:
            return @"neljännellä";
            break;
        default:
            return @"";
            break;
    }
}

- (void)viewDidUnload
{
    [self setLabel1:nil];
    [self setLabel2:nil];
    [self setApartmentSizeSlider:nil];
    [self setLabel3:nil];
    
    [self setMeanPriceLabel:nil];
    [self setSizeLabel:nil];
    [self setCopyrightLabel:nil];
    [self setSizeTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)aparmentSizeValueChanged:(id)sender
{
    if([sender isKindOfClass:[UISlider class]]) {
        
        UISlider *slider = (UISlider*)sender;
        float value = [slider value];
        
        if(infoDatapoint != nil){
            [label3 setText:[NSString stringWithFormat:@"%d", infoDatapoint.mean_price * (int)value]];
        }
        
        [sizeTextField setText:[NSString stringWithFormat:@"%d", (int)value]];
    }
    else if ([sender isKindOfClass:[UITextField class]]){
        
        int value = [[sizeTextField text] intValue];
        if(infoDatapoint != nil){
            [label3 setText:[NSString stringWithFormat:@"%d", infoDatapoint.mean_price * value]];
        }
        
        [apartmentSizeSlider setValue:(float)value];
    }
}

- (void) statButtonPressed
{
    GraphViewController *statViewController = [[GraphViewController alloc] initWithNibName:@"GraphViewController"
                                                                                    bundle:[NSBundle mainBundle]];
    [statViewController setAnnuals:annuals];
    [statViewController setQuarters:datapoints];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Takaisin" style:UIBarButtonItemStyleBordered target:nil action:nil];
    [backButton setTintColor:[UIColor colorWithRed:0.65f green:0.65f blue:0.65f alpha:1.0f]];
    [[self navigationItem] setBackBarButtonItem:backButton];
    [self.navigationController pushViewController:statViewController animated:YES];
}

@end
