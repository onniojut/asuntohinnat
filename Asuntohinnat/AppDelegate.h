//
//  AppDelegate.h
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 16.8.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//
#import "Crittercism.h"
#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
