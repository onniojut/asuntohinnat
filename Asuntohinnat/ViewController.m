//
//  ViewController.m
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 16.8.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//

#import "ViewController.h"
#import "ResultViewController.h"
#import "ASIHTTPRequest/ASIHTTPRequest.h"
#import "ASIHTTPRequest/Reachability/Reachability.h"
#import "SBJson.h"
#import "DataPoint.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize searchButton;
@synthesize agePicker;
@synthesize selectedAge = _selectedAge;
@synthesize selectedType = _selectedType;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // customize navigation bar
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 400, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:20.0f];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor =[UIColor whiteColor];
    label.text = @"Asuntohinnat";
    self.navigationItem.titleView = label;
//    [self.navigationItem.titleView sizeToFit]; 
    
    //[[self navigationItem] setTitle:@"Asuntohinnat"];
//    [[[self navigationController] navigationBar] setTintColor:[UIColor grayColor]];
//    [[[self navigationController] navigationBar] setTintColor:[UIColor colorWithRed:1.0f green:0.75f blue:0.0f alpha:1.0f]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nabar-bg"] forBarMetrics:UIBarMetricsDefault];
    
    [self.searchButton setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    [self.searchButton setBackgroundImage:[UIImage imageNamed:@"btn-hl"] forState:UIControlStateHighlighted];
    [self.searchButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.searchButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    self.selectedAge = 0;
    self.selectedType = 0;
    runningRequest = NO;
    [self hideIndicator];
}

- (void)viewWillAppear:(BOOL)animated
{
}

- (void)viewDidUnload
{
    [self setSearchButton:nil];
    [self setAgePicker:nil];
    postCodeTextField = nil;
    activityIndicator = nil;
    indicatorBackgroundView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)searchButtonPressed:(id)sender
{
    if(!runningRequest){
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        if([reachability isReachable]){
            // backend is reachable
            NSLog(@"We have internet connection!");
            
            if([self isValidPostcode:[postCodeTextField text]]){
                [self requestDataFromBackend];
            }
            else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Virheellinen postinumero." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alertView show];
            }
            
        }
        else{
            // display error message
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Ei yhteyttä Internetiin" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alertView show];
        }
    }
}

-(BOOL)isValidPostcode:(NSString*)postcode
{
    NSString *postcodeRegex = @"[0-9]{1,5}";
    NSPredicate *postcodeTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", postcodeRegex];
    
    return [postcodeTest evaluateWithObject:postcode];
}

- (IBAction)postcodeEditingDidEnd:(id)sender
{
    [sender resignFirstResponder];
}

- (IBAction)touchOnBackground:(id)sender
{
    [postCodeTextField resignFirstResponder];
}

#pragma mark UIPickerViewDataSource protocol
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == 0){
        return 8; // age
    }
    else{
        return 6; // types
    }
}

#pragma mark UIPickerViewDelegate

//Called by the picker view when the user selects a row in a component.
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(component == 0){
        self.selectedAge = row;
    }
    else{
        self.selectedType = row;
    }
}

// Called by the picker view when it needs the row height to use for drawing row content.
//- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
//{
//    return 20.0f;
//}

// Called by the picker view when it needs the title to use for a given row in a given component.
//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
//    UILabel *retval = (id)view;
//    if (!retval) {
//        retval= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
//    }
//    
//    retval.text = [self pickerView:pickerView titleForRow:row forComponent:component];
//    retval.font = [UIFont fontWithName:@"Helvetica" size:18.0f];
//    return retval;
//}

//- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    NSString *string = [self pickerView:pickerView titleForRow:row forComponent:component];
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
//    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,1)];
//    return attributedString;
//}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component == 0){
        switch (row) {
            case 1:
                return @"-1949";
                break;
            case 2:
                return @"1950-1959";
                break;
            case 3:
                return @"1960-1969";
                break;
            case 4:
                return @"1970-1979";
                break;
            case 5:
                return @"1980-1989";
                break;
            case 6:
                return @"1990-1999";
                break;
            case 7:
                return @"2000-";
                break;
            case 0:
                return @"Kaikki";
                break;
                
            default:
                return @"";
                break;
        }
    }
    else{
        switch (row) {
            case 1:
                return @"Yksiöt";
                break;
            case 2:
                return @"Kaksiot";
                break;
            case 3:
                return @"Kolmiot";
                break;
            case 4:
                return @"Kerrostalot";
                break;
            case 5:
                return @"Rivi-/Oma-";
                break;
            case 0:
                return @"Kaikki";
                break;
                
            default:
                return @"";
                break;
        }
    }
    
    //return [postcodes objectAtIndex:row];
}

// Called by the picker view when it needs the view to use for a given row in a given component.
//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
//{
//
//}

#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //[postCodeTextField becomeFirstResponder];
}

-(void)requestDataFromBackend
{
    runningRequest = YES;
    [self shotIndicator];
    
    NSString *appversion = [NSString stringWithFormat:@"%@ (%@)",
                            [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                            [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    
    NSString *urlString = [NSString stringWithFormat:@"%@?postcode=%@&age=%d&type=%d",
                           URL_DATA, [postCodeTextField text], self.selectedAge, self.selectedType];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setRequestMethod:@"GET"];
    [request setUserAgentString:[NSString stringWithFormat:@"Asuntohinnat iPhone app / %@", appversion]];
    //[request setAuthenticationScheme:(NSString *)kCFHTTPAuthenticationSchemeBasic];
    [request setDelegate:self];
    [request startAsynchronous];
}

#pragma mark ASIHTTPRequestDelegate
- (void)requestFinished:(ASIHTTPRequest *)request
{
    runningRequest = NO;
    [self hideIndicator];
    // TODO: check response validity
    
    // Use when fetching text data
    NSString *responseString = [request responseString];
    NSLog(@"http request successful with message: %@", responseString);
    
    NSMutableArray *datapoints = [NSMutableArray arrayWithCapacity:0];
    NSString *copyrightString = nil;
    
    @try {
        NSDictionary *dict = [responseString JSONValue];
        copyrightString = [dict objectForKey:@"copyright"];
        NSArray *arr = [dict objectForKey:@"result"];
        if(copyrightString != nil && arr != nil){
            for(NSDictionary *objDict in arr){
                [datapoints addObject:[[DataPoint alloc] initWithValuesFromDictionary:objDict]];
            }
            
            if(datapoints.count > 0){
                ResultViewController *resultViewController = [[ResultViewController alloc] initWithNibName:@"ResultViewController" bundle:[NSBundle mainBundle]];
                [resultViewController setDatapoints:datapoints];
                [resultViewController setCopyrightString:copyrightString];
                
                UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Takaisin" style:UIBarButtonItemStyleBordered target:nil action:nil];
                [backButton setTintColor:[UIColor colorWithRed:0.65f green:0.65f blue:0.65f alpha:1.0f]];
                [[self navigationItem] setBackBarButtonItem:backButton];
                
                [[self navigationController] pushViewController:resultViewController animated:YES];
            }
            else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Postinumerolle ei löytynyt hintatietoja." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alertView show];
            }
        }
        else{
            NSString *errorDescription = [dict objectForKey:@"error_description"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:errorDescription delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alertView show];
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        // Added to show finally works as well
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    runningRequest = NO;
    [self hideIndicator];
    
    NSError *error = [request error];
    NSLog(@"http request failed with error: %@", [error description]);
    
    int statusCode = [request responseStatusCode];
    
    if(statusCode == 403){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Pyyntöä ei voitu suorittaa, koska palvelun kapasiteetti on ylittynyt. Yritä myöhemmin uudelleen." delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alertView show];
    }
}

// If a delegate implements one of these, it will be asked to supply credentials when none are available
// The delegate can then either restart the request ([request retryUsingSuppliedCredentials]) once credentials have been set
// or cancel it ([request cancelAuthentication])
- (void)authenticationNeededForRequest:(ASIHTTPRequest *)request
{
    [request setUsername:@"FLYqXouWCb"];
    [request setPassword:@"iAq8dV6ZTt"];
    [request retryUsingSuppliedCredentials];
}
- (void)proxyAuthenticationNeededForRequest:(ASIHTTPRequest *)request
{
    [request setUsername:@"FLYqXouWCb"];
    [request setPassword:@"iAq8dV6ZTt"];
    [request retryUsingSuppliedCredentials];
}

-(void)hideIndicator
{
    [activityIndicator stopAnimating];
    [activityIndicator setHidden:YES];
    [indicatorBackgroundView setHidden:YES];
}

-(void)shotIndicator
{
    [activityIndicator startAnimating];
    [activityIndicator setHidden:NO];
    [indicatorBackgroundView setHidden:NO];
}


@end
