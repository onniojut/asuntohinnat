//
//  GraphViewController.h
//  Asuntohinnat
//
//  Created by Onni Ojutkangas on 17.9.2012.
//  Copyright (c) 2012 Onni Ojutkangas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlotHeaders/CorePlot-CocoaTouch.h"

@interface GraphViewController : UIViewController <CPTPlotDataSource, CPTAxisDelegate, CPTPlotSpaceDelegate>
{
    CPTXYGraph *quartersGraph;
    CPTXYGraph *annualsGraph;
    NSMutableArray *quarters;
    NSMutableArray *annuals;
    
    //ff
    int quartersStartYear;
    int quartersStartQuarter;
    int annualsStartYear;
}

@property (nonatomic, retain) NSMutableArray *quarters;
@property (nonatomic, retain) NSMutableArray *annuals;

@property (strong, nonatomic) IBOutlet CPTGraphHostingView *quartersGraphHostingView;
@property (strong, nonatomic) IBOutlet CPTGraphHostingView *annualsGraphHostingView;

- (void)segmentAction:(id)sender;
-(CGPoint)plotSpace:(CPTPlotSpace *)space willDisplaceBy:(CGPoint)proposedDisplacementVector;


@end
